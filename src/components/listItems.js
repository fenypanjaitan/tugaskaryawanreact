  
import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';


export const mainListItems = (
  <div>
    <ListItem button>
      <Link to="/">
      <ListItemText primary="List Data Karyawan" />
      </Link>
    </ListItem>

    <ListItem button>
    <Link to="/addData">
      <ListItemText primary="Add Data Karyawan" />
      </Link>
    </ListItem>
   
  </div>
);

