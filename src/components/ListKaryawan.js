import React, { Component } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button } from 'react-bootstrap';

class ListKaryawan extends Component {
  constructor(){
    super()
    this.state={
      karyawans: []
    }
  }
  componentDidMount() {
    axios.get(`http://localhost:8080/karyawans`)
      .then(res => {
        const karyawans = res.data;
        this.setState({ karyawans });
      })
      .catch(error=>{
        console.log(error);
        
      })
    }

    delete = id =>{
      axios.delete('http://localhost:8080/delete/'+id)
      .then(()=>{
        window.location.reload();
      })
      .catch(error =>{
        console.log(error);
        
      })
    }
  render() {
    return (
      <Table striped bordered hover>

        <thead>
          <tr>
            <th>Nama</th>
            <th>JenisKelamin</th>
            <th>Alamat</th>
            <th>Jabatan</th>
            <th>Gaji</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.state.karyawans.length? this.state.karyawans.map(data=>
          <tr>
            <td>{data.nama}</td>
            <td>{data.jeniskelamin}</td>
            <td>{data.alamat}</td>
            <td>{data.jabatan}</td>
            <td>{data.gaji}</td>
            <td>
              {/* <Button variant="outline-warning" onClick={()=>this.edit(data.id)}>edit</Button> */}
              <Button variant="outline-danger" onClick={()=>this.delete(data.id)}>Hapus</Button>  
            </td>
          </tr>
          ):
          <tr></tr>
        }
        </tbody>
    </Table>
           
       
    );
  }
}

export default ListKaryawan;