import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button } from 'react-bootstrap';
import axios from "axios";



class AddKaryawan extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            'nik':'',
            'nama':'',
            'tempatlahir':'',
            'tanggallahir':'',
            'jeniskelamin':'',
            'goldarah':'',
            'alamat':'',
            'rtrw':'',
            'keldesa':'',
            'kecamatan':'',
            'agama':'',
            'statusperkawinan':'',
            'pekerjaan':'',
            'kewarganegaraan':'',
            'jabatan':'',
            'gaji':''
         };
    }
    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
      };
    
      handleSubmit = (tambah) => {
        tambah.preventDefault();
        axios.post('http://localhost:8080/addKaryawan', this.state )
          .then(res => {
          console.log(res.data);
        });
      };
    render() { 
        return ( 
            <>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <Form.Label>NIK</Form.Label>
                        <Form.Control type="text" name="nik" placeholder="NIK" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Nama</Form.Label>
                        <Form.Control type="text" name="nama" placeholder="Nama" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Tempat Lahir</Form.Label>
                        <Form.Control type="text" name="tempatlahir" placeholder="Tempat Lahir" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Tanggal Lahir</Form.Label>
                        <Form.Control type="date" name="tanggallahir" placeholder="Tanggal Lahir" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Jenis Kelamin</Form.Label>
                        <Form.Control as="select" name="jeniskelamin" onChange={this.handleChange}>
                            <option>--Pilih--</option>
                            <option>Laki-laki</option>
                            <option>Perempuan</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Golongan Darah</Form.Label>
                        <Form.Control as="select" name="goldarah" onChange={this.handleChange}>
                            <option>-</option>
                            <option>A</option>
                            <option>B</option>
                            <option>AB</option>
                            <option>O</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Alamat</Form.Label>
                        <Form.Control type="text" name="alamat" placeholder="Alamat" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>RT/RW</Form.Label>
                        <Form.Control type="text" name="rtrw" placeholder="RT/RW" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Kelurahan/Desa</Form.Label>
                        <Form.Control type="text" name="keldesa" placeholder="Kelurahan/Desa" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Kecamatan</Form.Label>
                        <Form.Control type="text" name="kecamatan" placeholder="Kecamatan" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Agama</Form.Label>
                        <Form.Control as="select" name="agama" onChange={this.handleChange}>
                            <option>--Pilih--</option>
                            <option>Islam</option>
                            <option>Kristen</option>
                            <option>Katolik</option>
                            <option>Hindu</option>
                            <option>Budha</option>
                            <option>Konghuchu</option>
                            <option>lainnya</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Status Perkawinan</Form.Label>
                        <Form.Control as="select" name="statusperkawinan" onChange={this.handleChange}>
                            <option>--Pilih--</option>
                            <option>Belum Kawin</option>
                            <option>Kawin</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Pekerjaan</Form.Label>
                        <Form.Control type="text" name="pekerjaan" placeholder="pekerjaan" onChange={this.handleChange} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Kewarganegaraan</Form.Label>
                        <Form.Control type="text" name="kewarganegaraan" placeholder="Kewarganegaraan" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Jabatan</Form.Label>
                        <Form.Control type="text" name="jabatan" placeholder="Jabatan" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Gaji</Form.Label>
                        <Form.Control type="number" name="gaji" placeholder="Gaji" onChange={this.handleChange}/>
                    </Form.Group>

                    <Button variant="primary" type="submit"> Tambah Data </Button>
                    
                </Form>
            </>
         );
    }
}
 
export default AddKaryawan;