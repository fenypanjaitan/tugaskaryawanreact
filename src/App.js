import React from 'react';
import Dashboard from './components/Dashbord';
import { Switch, Route } from 'react-router-dom';
import Home from './components/Home';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" exact component={Dashboard}/>
        <Route path="/addData" component={Home}/>

      </Switch>
    </div>
  );
}

export default App;
